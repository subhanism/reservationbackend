const User = require('../models/user.js')
const jwt = require('jsonwebtoken')


exports.signup = (req, res) => {

	let  user=User.findOne( {email : req.body})
	
		if(user) return res.status(400).json({
			message : "User Already registered."
		})
	
	const  {
		 firstName ,
		 lastName  ,
		 email , 
		 password  , 
		 phoneNumber
		} = req.body
	const _user  = new User({
		 firstName ,
		 lastName  ,
		 email , 
		 password  ,
		 userName   : Math.random().toString() , 
		 phoneNumber

		})

	_user.save( (error , data) => {
		if(error)
			return res.status(400).json(
			{
				message : error
			});
		if(data)
			return res.status(201).json(
			{
				user :  data
			})
	})
}


exports.signin = (req , res ) => {
 try{

	User.findOne({email : req.body.email})
	.exec((error , user) => {
	if(error)	return res.status(400).json({ error })
	console.log("222");
	if(user) {
		if(user.authenticate(req.body.password)) {
			const token = jwt.sign({_id : user._id ,role : user.role} , process.env.JWT_SECRET , {expiresIn: '1h'})
			const  {_id , fullName , email , role } = user
			res.status(200).json({
				token , 
				user  : {_id , fullName , email , role }
			
			})


		} else {return res.status(400).send("Invalid Email or password. ")}
	}
		else{return res.status(400).send("Something went wrong")}
})
 }
 catch(err) {
 	console.log(err);
 }
}




// exports.requireSignin = (req, res , next) => {

// 	const token =req.headers.authorization.split(" ")[1]
// 	const user = jwt.verify(token , process.env.JWT_SECRET);
// 	req.user = user
// 	next()
// }