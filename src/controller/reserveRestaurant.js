const ReserveRestaurant = require('./../models/ReserveRestaurant')
const _ = require('lodash')
exports.reserveRestaurant = (req , res) => {
	let  user= ReserveRestaurant.findOne( {userId : req.userId  })
		console.log(user.bookingDate);
		if(user) return res.status(400).json({
			message : "your request is pending."
		})
	// _.pick(req.body , [ partySize , bookingDate , time , instruction , categoryId , restaurantId])
	const {partySize , bookingDate , time , instruction , categoryId , restaurantId , status} = req.body
	const reserveRestaurant = { 
		partySize,
		bookingDate,
		time,
		userId: req.user._id,
		instruction ,                                             
		categoryId , 
		restaurantId,
		status
	}
		const reserve = ReserveRestaurant(reserveRestaurant)
		reserve.save(((error , reserve)=> {
			if(error) return res.status(400).json({error})
			if(reserve) return res.status(200).json({reserve})
		}))

}