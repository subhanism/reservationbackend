const Restaurant = require('./../models/Restaurant.js')
const shortid = require('shortid')
const slugify = require('slugify')


exports.createRestaurant = (req ,res ) => {
	// res.status(200).json({file : req.files , body : req.body})
		const {name  , description ,  openTime , createdBy , categoryId
		 ,closeTime ,type ,phNumber , availableTable } = req.body
		 let restaurantPicture= [];

    if(req.files.length > 0){
        restaurantPicture = req.files.map(file => {
            return { img: file.filename }
        })
    }
			 
		const restaurant = {
			name: name ,
			slug: slugify(name) ,
			description,
			openTime ,
			createdBy: req.user._id,
			categoryId  ,
			closeTime ,
			type ,
			phNumber ,
			restaurantPicture, 
			availableTable
			
		}
		const rest = Restaurant(restaurant)
		rest.save(((error , product) => {
		
		if(error) return res.status(400).json({error})
		if(product){
		return res.status(201).json({product})
		}
	}))
}

exports.getAllRestaurant = (req ,res) => {
	Restaurant.find({}).exec((error , restaurant) => {
		if(error) return res.status(400).json({error})
		if(restaurant) return res.status(200).json({
			restaurant
		})
	})
}

exports.getRestaurant = (req ,res) => {
	console.log(req.params.name);
	const restaurant = Restaurant.find({name : req.params.name})
	.select('name, description , type ')
	.exec((error , restaurant) => {
		if(error) return res.status(400).json({error})
		if(restaurant) return res.status(200).json({
			restaurant
		})
	})
}