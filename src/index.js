const express = require('express')
const app = express()
const env = require('dotenv')
// const bodyParser = require('body-parser')
const userRouter = require("./routes/auth.js")
const categoryRouter = require("./routes/category.js")
const reserveRestaurantRouter = require("./routes/reserveRestaurant.js")
const adminRouter = require("./routes/admin/auth.js")
const restaurantRouter = require("./routes/restaurant.js")
require('./db/conn.js')()


env.config()	



app.use(express.urlencoded({ extended: true }))
app.use(express.json());

// Routes
app.use("/api" , userRouter)
app.use("/api" , adminRouter)
app.use("/api" , categoryRouter)
app.use("/api" , restaurantRouter)
app.use("/api" , reserveRestaurantRouter)
 

const port = process.env.PORT 
app.listen(port ,
 ()=> console.log(`App is running on ${port}`))



