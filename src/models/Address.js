const mongoose = require('mongoose')

const addressSchema = mongoose.Schema({
		shopNumber : {type: String,
		required : true ,
		},
		blockNumber : {type: String,
		required : true ,
		},
		area : {type: String,
		required : true ,
		},
		city : {type: String,
		required : true ,
		},

})

const Address = mongoose.model('Address', addressSchema)

module.exports  = Address