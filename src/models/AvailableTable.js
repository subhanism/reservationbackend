const mongoose = require('mongoose')

const availableTableSchema = mongoose.Schema({

	restaurantId : {type : mongoose.Schema.Types.ObjectId ,
					ref : 'Restaurant' , required: true} ,
	chair : {type : Number , require: true}
	table : {type : Number , require: true} , 
	reservationTime : {type : String  ,
	enum : ['7.30:pm' ,'8.00:pm' , '8.30:pm' , '9.00:pm', '9.30:pm' , '10.00:pm'] }
}) 
const AvailableTable = mongoose.model( 'AvailableTable' , availableTableSchema)

module.exports = AvailableTable