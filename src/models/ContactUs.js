const mongoose = require('mongoose')
const ContactSchema = new mongoose.Schema({
    message: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true },
    phoneNumber: { type: String, required: true },
})
const Contact = mongoose.model('ContactSchema', ContactSchema)

module.exports = Contact
