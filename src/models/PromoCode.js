const mongoose = require('mongoose')
const Schema = mongoose.Schema

const promoCodeSchema = new Schema({
    promoCode: {
        type: String,
    },

    status: {
        type: String,
        enum: ['active', 'inactive'],
        default: 'active',
    },

    discountPercentage: {
        type: Number,
        default: 0,
    },

    fixedAmount: {
        type: Number,
        default: 0,
    },

    usedBy: {
        type: Number,
        default: 0,
    },

    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() },
})
promoCodeSchema.set('toJSON')

promoCodeSchema.index({ createdAt: 1 })
promoCodeSchema.index({ createdAt: -1 })

promoCodeSchema.index({ promoCode: 1 })
promoCodeSchema.index({ promoCode: -1 })

promoCodeSchema.index({ status: 1 })
promoCodeSchema.index({ status: -1 })

promoCodeSchema.index({ usedBy: 1 })
promoCodeSchema.index({ usedBy: -1 })

promoCodeSchema.index({ discountPercentage: 1 })
promoCodeSchema.index({ discountPercentage: -1 })

/** Exported Module*/
module.exports = mongoose.model('PromoCode', promoCodeSchema, 'promoCodes')
