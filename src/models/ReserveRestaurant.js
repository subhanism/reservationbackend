const mongoose = require('mongoose')

const reserveRestaurantSchema = mongoose.Schema({
userId :{type :mongoose.Schema.Types.ObjectId, ref:'user',required : true} ,
partySize :{
	type : Number , 
	required : true ,
},
bookingDate : {
	type : String , 
	required : true ,
} , 
time : {
	type : String , 
	required : true ,
},
status : {
	type : String , 
	enum : ['pending' , 'confrim' , 'finished' ,'cancelled'] ,
	default : 'pending'
} , 
instruction : String ,
categoryId :{type :mongoose.Schema.Types.ObjectId, ref:'category',required : true} ,
restaurantId :{type :mongoose.Schema.Types.ObjectId, ref:'Restaurant',required : true} ,


	
} , {timestamps : true})

const ReserveRestaurant = mongoose.model('ReserveRestaurant' , reserveRestaurantSchema)

module.exports = ReserveRestaurant;
