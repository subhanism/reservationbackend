const mongoose = require('mongoose')

const restaurantSchema = mongoose.Schema({
	name : {
		type : String , 
		trim : true , 
		required : true ,
		unique : true
	} ,
	slug : {
		type : String , 
		trim : true , 
		required : true ,
		unique : true
	} , 

	description : {
		type : String  ,
		required : true ,

	} ,
	openTime : {
		type : String ,
		required: true
	},
	closeTime : {
		type : String ,
		required: true
	} , 
	type : {
		type : String ,
		enum: ['fast food' , 'barbecue' , 'italian' ,'indian', 'pakistani'], 
		required: true
	} ,
	phNumber : {
		type : Number  ,
		required : true ,
	} ,
	restaurantPicture : [{img : String}] ,
	reviews : [ {
		userId :{type :  mongoose.Schema.Types.ObjectId  , ref : 'User' },
		review : String 
	}] , 
	availableTable :  {type :Number , required :true} ,
	menuPicture : [{img : String}],
	isAcative : {type : Boolean , default : true} ,

	categoryId  :{ type : mongoose.Schema.Types.ObjectId ,
				 ref : 'Category' ,
				 required : true} ,
	createdBy  :{ type : mongoose.Schema.Types.ObjectId , ref : 'User' , required : true} ,
	UpdatedAy : { Date}
	
} , {timestamps : true})

const Restaurant = mongoose.model('Restaurant' , restaurantSchema)

module.exports = Restaurant;
