const mongoose = require('mongoose')

const categorySchema = mongoose.Schema( {
	name : {
		type : String , 
		require : true , 
		trim : true 	
		} ,
	slug : {
		type : String , 
		require : true , 
		unique : true 	
		} , 
	categoryId : {
		type  :String 
	}

} , { timestamps : true})

const Category = mongoose.model('Category ', categorySchema)

module.exports = Category