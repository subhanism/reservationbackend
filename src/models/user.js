const mongoose = require("mongoose")
const bcrypt = require('bcrypt')
const userSchema = mongoose.Schema({

	firstName :{
		type : String  , 
		min : 3 , 
		max : 20 ,
		trim  : true ,
		required: true
		} , 
	lastName :{
		type : String  , 
		min : 3 , 
		max : 20 ,
		trim  : true ,
		required: true
		} ,
	userName :{
		type : String  , 
		min : 3 , 
		max : 20 ,
		trim  : true ,
		required: true ,
		lowercase : true , 
		unique  : true ,
		index : true 
		} , 
	email :{
		type : String  , 
		required: true , 
		trim : true , 
		lowercase : true , 
		unique : true 
		} ,
	hash_password :  {
		required: true , 
		type : String
		} , 
	role  : {
		type : String , 
		enum: ['user' , 'admin'], 
		default : 'user'
		} ,
	phoneNumber : {
		required : true ,
		type : Number
		} ,
	profilePicture : {
		type : String 
	},
	status: {
            type: String,
            required: true,
            enum: ['active', 'cancel', 'pending', 'inactive'],
            default: 'pending',
        }
} ,{timestamps: true })

	userSchema.virtual('password')
	.set(function(password) {
		 // const salt =  bcrypt.genSalt(10);	
		this.hash_password = bcrypt.hashSync(password , 10)
	});

// to check plain text password into our database hash_password
	userSchema.methods = {
		authenticate  : function(password) {
			return bcrypt.compareSync(password , this.hash_password)
		}
	}
	userSchema.virtual('fullname')
	.get( function() {
		return `${this.firstName} ${this.lastName}`
	})

const User = mongoose.model('User' , userSchema)
module.exports = User