const express = require('express')
const router = express.Router()
const {signup , signin } = require('../../controller/admin/auth.js')
const { validateSignUpRequest , isRequestValidated , validateSignInRequest} = require('./../../validators/auth.js')

// import {signup} from "../controller/auth.js";

router.post("/admin/signup"  , validateSignUpRequest , isRequestValidated , signup);
router.post("/admin/signin" , validateSignInRequest , isRequestValidated , signin);

module.exports = router

//wow