const express = require('express')
const router = express.Router()
const {signup , signin ,requireSignin} = require('../controller/auth.js')
const {validateSignUpRequest, validateSignInRequest , isRequestValidated} = require('./../validators/auth.js')
// import {signup} from "../controller/auth.js";

router.post("/signup"  , validateSignUpRequest , isRequestValidated, signup);
router.post("/signin" , validateSignInRequest , isRequestValidated ,signin);
// router.post("/admin/profile" , requireSignin ,(req ,res) => {
// 	res.status(200).json({user :  'profile'})
// })

module.exports = router
