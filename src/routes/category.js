const {addCategory , getCategory } = require('./../controller/category')
const {requireSignin , middlewareAdmin} = require('./../middleware/index.js')
const express = require('express') 
const slugify = require('slugify') 
const router = express.Router()

router.post("/category/create",requireSignin , middlewareAdmin ,addCategory );
router.get("/category/getCategory",requireSignin ,  getCategory );

module.exports = router
