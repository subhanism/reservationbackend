const { reserveRestaurant} = require('./../controller/reserveRestaurant.js')
const {requireSignin , middlewareUser} = require('./../middleware/index.js')
const express = require('express') 
const slugify = require('slugify') 
const router = express.Router()

router.post("/user/reservation/restaurant",requireSignin , middlewareUser ,reserveRestaurant );
//router.get("/user/reservation/restaurant",requireSignin ,  getCategory );

module.exports = router
