const {requireSignin , middlewareAdmin} = require('./../middleware/index.js')
const express = require('express') 
const slugify = require('slugify') 
const multer = require('multer') 
const shortid = require('shortid') 
const path = require('path') 
// const upload = multer({dest : 'uploads/'})
const router = express.Router()
const {createRestaurant ,getAllRestaurant,  getRestaurant} = require('../controller/restaurant.js')

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(path.dirname(__dirname) , 'uploads'))
  },
  filename: function (req, file, cb) {
    cb(null, shortid.generate() + '-'  + file.originalname)
  }
})
 const upload = multer({storage})
router.post('/restaurant/create',requireSignin ,middlewareAdmin
			,upload.array('restaurantPicture'), createRestaurant);	

router.get("/restaurant/allRestaurant",requireSignin,
			getAllRestaurant);

router.get("/restaurant/:name",requireSignin,
			getRestaurant);

module.exports = router
